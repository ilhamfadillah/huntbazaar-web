# How to install
1. Clone this repository
2. copy .env.example then rename with .env

> change value APP_URL with http://localhost:{active port}, usually the port used is 8000, this will take effect when the link is sent to the email, if only http://localhost without a port, then the link in the email will not work, unless we change it ourselves in the address bar with http://localhost:{port}

> create a table with the name huntbazaar, then adjust the username and password in .env according to your MySQL settings

- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=huntbazaar
- DB_USERNAME=
- DB_PASSWORD=

>Adjust the email credentials you use in this variable, I've tried it using Mailtrap

- MAIL_MAILER=smtp
- MAIL_HOST=smtp.mailtrap.io
- MAIL_PORT=2525
- MAIL_USERNAME=
- MAIL_PASSWORD=
- MAIL_ENCRYPTION=tls


3. open your terminal, navigate to where the clone file is saved
4. Run the command **composer install**
5. Run command **npm install**
6. Run command **npm run dev**
7. Run the commane **php artisan key:generate**
8. Run the command **php artisan migrate**
9. Run the command **php artisan db:seed**
10.  Run the command **php artisan serve** and open the link that has been generated
11.  Here are some links that can be used


>**http://localhost:{port}/** is MainPage

>**http://localhost:{port}/admin** is login to admin page, the account that can be used for admin login is (email: admin@admin.com & password: password)

>**http://localhost:{port}/invitation** is a page where you can add an invite and see who has been sent an invitation

>**http://localhost:{port}/invitation_guest** is a page to see who has accepted the invitation

> **http://localhost:{port}/invitation/{token}** is a page that must be filled out by the recipient of the invitation, as a sign to accept the invitation

12.  Running the command **php artisan queue:listen** to run laravel job, used to send email in local