<?php

namespace Database\Seeders;

use App\Models\Designer;
use App\Models\InvitationGuest;
use App\Models\Status;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class InvitationGuestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');
        $invitations = Status::find(4)->invitations;
        foreach ($invitations as $invitation) {
            if (!$invitation->invitationGuest()->exists()) {
                $designerId = Designer::inRandomOrder()->get()->take(rand(1, 15))->pluck('id');
                $invitation->invitationGuest()->create([
                    "name" => $faker->name(),
                    "date_of_birth" => $faker->date(),
                    "gender" => $faker->randomElement(['male', 'female'])
                ]);
                $invitation->invitationGuest->designers()->sync($designerId);
            }
        }
    }
}
