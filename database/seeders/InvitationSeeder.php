<?php

namespace Database\Seeders;

use App\Jobs\GuestRegistrationJob;
use App\Jobs\ThankYouEmailJob;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Invitation;
use Faker\Factory as Faker;

class InvitationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!Invitation::exists()) {
            $faker = Faker::create('id_ID');
            $data = [];
            for ($i=0; $i < 10; $i++) {
                $email = $faker->email();
                $token = "Bazaar-" . Str::random(5);
                $status = rand(1, 4);
                $data[] = [
                    "status_id" => $status,
                    "email" => $email,
                    "token" => $token,
                    "created_at" => now(),
                    "updated_at" => now()
                ];
                $detail['email'] = $email;
                $detail['token'] = $token;
                if ($status == 1) {
                    dispatch(new ThankYouEmailJob($detail));
                }

                if ($status == 4) {
                    $delay = \Carbon\Carbon::now()->addMinutes(60);
                    dispatch(new GuestRegistrationJob($detail))->delay($delay);
                }
            }
            Invitation::insert($data);
        }
    }
}
