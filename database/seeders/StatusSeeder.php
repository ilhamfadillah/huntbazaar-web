<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Status::firstOrCreate([
            "id" => 1,
            "name" => "not sent"
        ]);
        Status::firstOrCreate([
            "id" => 2,
            "name" => "sent"
        ]);
        Status::firstOrCreate([
            "id" => 3,
            "name" => "opened"
        ]);
        Status::firstOrCreate([
            "id" => 4,
            "name" => "accepted"
        ]);
    }
}
