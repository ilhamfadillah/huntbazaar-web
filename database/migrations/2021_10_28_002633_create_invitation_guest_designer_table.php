<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationGuestDesignerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_guest_designer', function (Blueprint $table) {
            $table->unsignedBigInteger('invitation_guest_id');
            $table->unsignedBigInteger('designer_id');
            $table->foreign('invitation_guest_id')->references('id')->on('invitation_guests');
            $table->foreign('designer_id')->references('id')->on('designers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_guest_designer');
    }
}
