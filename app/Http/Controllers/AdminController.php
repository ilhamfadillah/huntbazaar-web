<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
        if (Auth::check()) {
            return redirect(route('invitation'));
        }
        return view('admin.login');
    }

    public function login(LoginRequest $request)
    {
        $credentials = $request->only("email", "password");
        if (Auth::attempt($credentials)) {
            return redirect()->intended('invitation');
        }
        return redirect()->back()->withErrors(['message' => 'You have entered an invalid email or password']);
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
