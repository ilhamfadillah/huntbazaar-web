<?php

namespace App\Http\Controllers;

use App\Http\Requests\InvitationGuestPostRequest;
use App\Http\Requests\InvitationPostRequest;
use App\Jobs\ThankYouEmailJob;
use App\Models\Designer;
use Illuminate\Support\Str;
use App\Models\Invitation;
use App\Models\Status;
use Illuminate\Http\Request;

class InvitationController extends Controller
{

    public function index()
    {
        $invitations = Invitation::paginate(10);
        $notSent = Status::find(1)->invitations()->count();
        $sent = Status::find(2)->invitations()->count();
        $opened = Status::find(3)->invitations()->count();
        $accepted = Status::find(4)->invitations()->count();
        return view('admin.invitation', [
            "invitations" => $invitations,
            "notSent" => $notSent,
            "sent" => $sent,
            "opened" => $opened,
            "accepted" => $accepted,
        ]);
    }

    public function store(InvitationPostRequest $request)
    {
        $status = Status::find(1);
        foreach ($request->email as $email) {
            $token = "Bazaar-" . Str::random(5);
            $post = $status->invitations()->create([
                "email" => $email,
                "token" => $token,
            ]);
            $detail['email'] = $email;
            $detail['token'] = $token;
            dispatch(new ThankYouEmailJob($detail));
        }
        return redirect()->route('invitation')->with('success','Invitation successfully added.');
    }

    function guestRegistration(Invitation $token)
    {
        $startdate = "10-October-2021";
        $expire = strtotime($startdate. ' + 1 months');
        $today = strtotime("today midnight");
        if ($today > $expire) {
            abort(404);
        }
        if ($token->status_id < 4) {
            $token->status_id = 3;
            $token->save();
        }
        return view('guest_registration', [
            "token" => $token,
            "designers" => Designer::orderBy("name")->get()
        ]);
    }

    function guestRegistrationStore(InvitationGuestPostRequest $request, Invitation $token)
    {
        $token->invitationGuest()->firstOrCreate($request->only(['name', 'date_of_birth', 'gender']));
        $token->invitationGuest->designers()->sync($request->designer);
        $token->status_id = 4;
        $token->save();
        $detail['email'] = $token->email;
        $detail['token'] = $token->token;
        $delay = \Carbon\Carbon::now()->addMinutes(60);
        dispatch(new ThankYouEmailJob($detail))->delay($delay);
        return redirect()->route('guest_registration', $token->token)->with('success','Thank You.');
    }
}
