<?php

namespace App\Http\Controllers;

use App\Models\Designer;
use App\Models\Invitation;
use App\Models\InvitationGuest;
use Illuminate\Http\Request;

class InvitationGuestController extends Controller
{
    public function index()
    {
        $invitationGuests = InvitationGuest::paginate(10);
        $topDesigners = Designer::whereHas("invitationGuests")
            ->withCount("invitationGuests")
            ->orderBy("invitation_guests_count", "desc")
            ->take(10)
            ->get();
        $minDate = InvitationGuest::orderBy("date_of_birth", "desc")->first()->age();
        $maxDate = InvitationGuest::orderBy("date_of_birth", "asc")->first()->age();
        $maleTotal = InvitationGuest::where("gender", "male")->count();
        $femaleTotal = InvitationGuest::where("gender", "female")->count();
        return view("admin.invitation_guest", [
            "topDesigners" => $topDesigners,
            "invitationGuests" => $invitationGuests,
            "minDate" => $minDate,
            "maxDate" => $maxDate,
            "maleTotal" => $maleTotal,
            "femaleTotal" => $femaleTotal
        ]);
    }
}
