<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvitationPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "email.*" => "required|email|unique:invitations,email"
        ];
    }

    public function messages()
    {
        $messages = [
            "email.*.required" => "Email is required"
        ];
        foreach ($this->get('email') as $key => $value) {
            $messages["email.$key.unique"] = $value . " has already been taken.";
        }
        return $messages;

    }
}
