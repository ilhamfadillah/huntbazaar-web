<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Designer extends Model
{
    use HasFactory;

    public function invitationGuests()
    {
        return $this->belongsToMany(InvitationGuest::class, 'invitation_guest_designer');
    }
}
