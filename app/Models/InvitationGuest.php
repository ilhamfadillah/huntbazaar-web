<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationGuest extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'date_of_birth', 'gender'];

    /**
     * Accessor for Age.
     */
    public function age()
    {
        return Carbon::parse($this->attributes['date_of_birth'])->age;
    }

    public function invitation()
    {
        return $this->belongsTo(Invitation::class);
    }

    public function designers()
    {
        return $this->belongsToMany(Designer::class, 'invitation_guest_designer');
    }
}
