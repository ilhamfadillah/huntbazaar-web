<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    protected $fillable = ['status_id', 'email', 'token'];

    public function invitationGuest()
    {
        return $this->hasOne(InvitationGuest::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
