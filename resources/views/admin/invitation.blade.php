@extends('master')

@section('content')

@include('admin.component.navbar')
<div class="container-fluid bg-white">
    <div class="row">
        <div class="col-4 pt-4">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger">
                @foreach ($errors->all() as $error)
                <div>{{$error}}</div>
                @endforeach
            </div>
            @endif
            <form action="{{ route('invitation.post') }}" method="post">
                @csrf
                <div class="card">
                    <div class="card-header bg-primary text-light font-weight-bold">
                        ADD INVITATION
                    </div>
                    <div class="card-body">
                        @if(old('email') != NULL)
                            @foreach(old('email') as $key => $value)
                            <div class="input-group" id="input-invitation-{{ $key }}">
                                <input type="email" value="{{ $value }}" name="email[]" class="form-control border-dark text-invitation" id="text-{{ $key }}" placeholder="Enter email">
                                <div class="input-group-append">
                                    <button type="button" class="input-group-text bg-danger text-white border-danger btn-remove" id="remove-{{ $key }}">X</button>
                                </div>
                            </div>
                            @endforeach
                        @else
                            <div class="input-group" id="input-invitation-1">
                                <input type="email" name="email[]" class="form-control border-dark text-invitation" id="text-1" placeholder="Enter email">
                                <div class="input-group-append">
                                    <button type="button" class="input-group-text bg-danger text-white border-danger btn-remove" id="remove-1">X</button>
                                </div>
                            </div>
                        @endif
                        <div class="input-group pt-1" id="add-input-parent">
                            <button type="button" class="btn btn-light border-dark btn-block" id="add-input">+</button>
                        </div>
                    </div>
                    <div class="card-footer" id="btn-submit-parent">
                        <button type="submit" class="btn btn-primary" id="btn-submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-8 pt-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Not Sent: {{ $notSent }}</th>
                        <th>Sent: {{ $sent }}</th>
                        <th>Opened: {{ $opened }}</th>
                        <th>Accepted: {{ $accepted }}</th>
                    </tr>
                </thead>
            </table>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">No</th>
                        <th scope="col">Email</th>
                        <th scope="col">Token</th>
                        <th scope="col">Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invitations as $key => $invitation)
                    <tr>
                        <td>{{ ($key + 1) }}</td>
                        <td>{{ $invitation->email }}</td>
                        <td>{{ $invitation->token }}</td>
                        <td>{{ $invitation->status->name }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {{ $invitations->links() }}
            </div>
        </div>
    </div>
</div>
@endSection

@section('script')
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $("form").on("click", "#add-input", function() {
        var order = 1;
        $("div[id^='input-invitation-']").each(function(index, value) {
            $(value).attr("id", "input-invitation-" + order);
            $(value).find("input[name='email[]']").attr("id", "text-" + order);
            $(value).find(".btn-remove").attr("id", "remove-" + order);
            order++;
        });

        if (order > 0 && $("#btn-submit").length == 0) {
            var btnSubmit = '<button type="submit" class="btn btn-primary" id="btn-submit">Submit</button>';
            $("#btn-submit-parent").html(btnSubmit);
        }

        var html = '<div class="input-group" id="input-invitation-' + order + '">'+
            '<input type="email" name="email[]" class="form-control border-dark" id="text-' + order + '" placeholder="Enter email">'+
            '<div class="input-group-append">'+
            '<button type="button" class="input-group-text bg-danger text-white border-danger btn-remove" id="remove-' + order + '">X</button>'+
            '</div></div>';
        $(html).insertBefore("#add-input-parent");
    });

    $("form").on("click", ".btn-remove", function() {
        var order = $(this).attr("id").split("-").pop();
        $("div[id='input-invitation-" + order + "']").remove();
        if ($("div[id^='input-invitation-']").length == 0) {
            $("#btn-submit").remove();
        }
    })
</script>
@endSection
