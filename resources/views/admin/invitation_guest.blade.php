@extends('master')

@section('content')

@include('admin.component.navbar')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 pt-4">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="align-top">
                            Top 10 Favorite Designer
                            <ol>
                                @foreach($topDesigners as $designer)
                                <li>{{ $designer->name }}</li>
                                @endforeach
                            </ol>
                        </th>
                        <th class="align-top">
                            Range Age
                            <div>{{ $minDate }} - {{ $maxDate }}</div>
                        </th>
                        <th class="align-top">
                            Male
                            <div>{{ $maleTotal }}</div>
                        </th>
                        <th class="align-top">
                            Female
                            <div>{{ $femaleTotal }}</div>
                        </th>
                    </tr>
                </thead>
            </table>
            <table class="table table-stripped">
                <thead>
                    <tr>
                        <th scope="col">Token</th>
                        <th scope="col">Email</th>
                        <th scope="col">Name</th>
                        <th scope="col">Age</th>
                        <th scope="col">Gender</th>
                        <th scope="col">Favorite Designer</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($invitationGuests as $key => $guest)
                    <tr>
                        <td>{{ $guest->invitation->token }}</td>
                        <td>{{ $guest->invitation->email }}</td>
                        <td>{{ $guest->name }}</td>
                        <td>{{ $guest->age() }}</td>
                        <td>{{ $guest->gender }}</td>
                        <td>
                            <ul>
                                @foreach($guest->designers()->orderBy('name')->pluck('name') as $value)
                                <li>{{ $value }}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="d-flex justify-content-center">
                {{ $invitationGuests->links() }}
            </div>
        </div>
    </div>
</div>
@endSection

@section('script')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
@endSection
