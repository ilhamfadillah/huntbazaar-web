<nav class="navbar navbar-expand-lg navbar-dark bg-dark font-weight-bold">
    <a class="navbar-brand" href="#">HUNTBAZAAR ADMIN</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item @if(Route::is('invitation')) active @endif">
                <a class="nav-link" href="{{ route('invitation') }}">Invitation</a>
            </li>
            <li class="nav-item @if(Route::is('invitation_guest')) active @endif">
                <a class="nav-link" href="{{ route('invitation_guest') }}">Guest</a>
            </li>
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="{{ route('logout') }}">Logout</a>
            </li>
        </ul>
    </div>
</nav>