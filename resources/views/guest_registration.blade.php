@extends('master')

@section('style')
<style>
    body {
        background: url(https://cdn.popbela.com/content-images/post/20190529/huntstreet-flagship-toko-tampak-depan-2-4dea03a7162a0c8df441a83243e6575f.JPG);
        background-size: cover;
    }

    #background-opacity {
        position: absolute;
        width: 100vw;
        height: 100vh;
        top: 0;
        left: 0;
        background-color: black;
        opacity: 0.5;
    }

</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endSection

@section('content')
<div id="background-opacity"></div>
<div class="container-fluid">
    <div class="row justify-content-center pt-4">
        <div class="col-8">
            <div class="card">
                <div class="card-header">
                    Expired In: <span id="countdown"></span>
                </div>
                <div class="card-body">
                    @if($token->invitationGuest == null)
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                        <div>{{$error}}</div>
                        @endforeach
                    </div>
                    @endif
                    <form action="{{ route('guest_registration.post', $token->token) }}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                <input type="text" readonly class="form-control-plaintext" id="staticEmail"
                                    value="{{ $token->email }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" @if(old("name")) value="{{ old('name') }}" @endif placeholder="Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Birth Date</label>
                            <div class="col-sm-10">
                                <input type="date" @if(old("date_of_birth")) value="{{ old('date_of_birth') }}" @endif name="date_of_birth" class="form-control" placeholder="Birth Date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Gender</label>
                            <div class="col-sm-10">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="radio-male" name="gender"
                                        value="male" @if(old("gender") && old("gender") == "male") checked  @endif>
                                    <label class="form-check-label" for="radio-male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="radio-female" name="gender"
                                        value="female" @if(old("gender") && old("gender") == "female") checked  @endif>
                                    <label class="form-check-label" for="radio-female">Female</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputPassword" class="col-sm-2 col-form-label">Favorite Designer</label>
                            <div class="col-sm-10">
                                <select class="form-control select-2" name="designer[]" multiple="multiple">
                                    @foreach($designers as $designer)
                                    @if (old('designer') != null)
                                    <option value="{{ $designer->id }}" @if (in_array($designer->id, old('designer'))) selected @endif>
                                        {{ $designer->name }}
                                    </option>
                                    @else
                                    <option value="{{ $designer->id }}">{{ $designer->name }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary float-right">Submit</button>
                            </div>
                        </div>
                    </form>
                    @else
                    {{ $token->token }}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endSection

@section('script')
<script src="//ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function () {
        $('.select-2').select2();
        var end = new Date('10/27/2021');
        end.setMonth(end.getMonth() + 1);

        var _second = 1000;
        var _minute = _second * 60;
        var _hour = _minute * 60;
        var _day = _hour * 24;
        var timer;

        function showRemaining() {
            var now = new Date();
            var distance = end - now;
            if (distance < 0) {

                clearInterval(timer);
                document.getElementById('countdown').innerHTML = 'EXPIRED!';

                return;
            }
            var days = Math.floor(distance / _day);
            var hours = Math.floor((distance % _day) / _hour);
            var minutes = Math.floor((distance % _hour) / _minute);
            var seconds = Math.floor((distance % _minute) / _second);

            document.getElementById('countdown').innerHTML = days + 'days ';
            document.getElementById('countdown').innerHTML += hours + 'hrs ';
            document.getElementById('countdown').innerHTML += minutes + 'mins ';
            document.getElementById('countdown').innerHTML += seconds + 'secs';
        }

        timer = setInterval(showRemaining, 1000);
    });

</script>
@endSection
