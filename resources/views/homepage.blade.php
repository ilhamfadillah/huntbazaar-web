@extends('master')

@section('style')
<style>
    #background-opacity-dark {
        position: fixed;
        width: 100%;
        height: 100%;
        background-color: black;
        opacity: 0.5;
        top: 0;
        left: 0;
    }

    video {  
        width: 100vw;
        height: 100vh;
        object-fit: cover;
        position: fixed;
        top: 0;
        left: 0;
        z-index: -1;
    }

    .border-width-10 {
        border-width: 10px !important;
    }

    .height-100 {
        height: 100vh;
    }

    .font-display {
        font-size: 5vw;
        font-weight: bold;
    }

</style>
@endSection

@section('content')
<div class="container-fluid px-0">
    <video autoplay muted loop id="myVideo">
        <source src="{{ asset('videoplayback.mp4') }}" type="video/mp4">
        Your browser does not support HTML5 video.
    </video>
    <div id="background-opacity-dark">
        <div class="row justify-content-center align-items-center height-100">
            <div class="col-11 col-sm-10 col-md-8  text-light">
                <div class="font-display text-center">HUNTBAZAAR</div>
                <div class="font-display text-center">12 Desember 2021</div>
                <div class="font-display text-center">HUNTSTREET.COM</div>
            </div>
        </div>
    </div>
</div>

@endSection
