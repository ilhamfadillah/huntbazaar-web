<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        @font-face {
            font-family: Roboto;
            src: url("{{ asset('font/roboto/Roboto-Light.ttf') }}");
        }
        body {
            font-family: Roboto;
        }
    </style>
    @yield('style')
</head>
<body>
    @yield('content')

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->
    @yield('script')
</body>
</html>