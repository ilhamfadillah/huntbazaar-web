<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\InvitationGuestController;
use App\Http\Controllers\MainPageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainPageController::class, 'index']);
Route::get('/admin', [AdminController::class, 'index']);
Route::post('/admin', [AdminController::class, 'login'])->name('login');
Route::get('/admin/logout', [AdminController::class, 'logout'])->name('logout');
Route::get('/invitation/{token:token}', [InvitationController::class, 'guestRegistration'])->name('guest_registration');
Route::post('/invitation/{token:token}', [InvitationController::class, 'guestRegistrationStore'])->name('guest_registration.post');

Route::middleware(['auth'])->group(function() {
    Route::get('/invitation', [InvitationController::class, 'index'])->name('invitation');
    Route::post('/invitation', [InvitationController::class, 'store'])->name('invitation.post');
    Route::get('/invitation_guest', [InvitationGuestController::class, 'index'])->name('invitation_guest');
});


// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
